import { Component, OnInit } from '@angular/core';
import {ServerService} from '../server.service';

@Component({
  selector: 'app-new-course-form',
  templateUrl: './new-course-form.component.html',
  styleUrls: ['./new-course-form.component.css']
})
export class NewCourseFormComponent{

  categories = [
    {id: 1, name: 'Development'},
    {id: 2, name: 'Arts'},
    {id: 3, name: 'Languages'},
  ];

  constructor(private serverService: ServerService){}

  onSave(){
    this.serverService.storeServers(this.categories)
      .subscribe(
        (response) => console.log(response),
        (error) => console.log(error)
      );
  }

  submit(course){
    console.log(course);
  }

}
