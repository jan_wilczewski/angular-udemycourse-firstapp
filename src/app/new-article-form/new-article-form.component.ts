import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'new-article-form',
  templateUrl: './new-article-form.component.html',
  styleUrls: ['./new-article-form.component.css']
})
export class NewArticleFormComponent{

  categories = [
    {id: 1, name: 'Development'},
    {id: 2, name: 'Java'},
    {id: 3, name: 'Angular'},
    {id: 4, name: 'HR'},
    {id: 5, name: 'Sodexo'},
    {id: 6, name: 'Firs Days'},
    {id: 7, name: 'Trainings'},
    {id: 8, name: 'Documents'},

  ];

  submit(course){
    console.log(course);
  }

}
