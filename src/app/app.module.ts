import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { TextEditorComponent } from './text-editor/text-editor.component';
import { TitleCasePipe } from './title-case.pipe';
import { PanelComponent } from './panel/panel.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { NewCourseFormComponent } from './new-course-form/new-course-form.component';
import { NewArticleFormComponent } from './new-article-form/new-article-form.component';
import {ServerService} from './server.service';
import { TextComponent } from './text/text.component';
import {QuillModule} from 'ngx-quill';



@NgModule({
  declarations: [
    AppComponent,
    TextEditorComponent,
    TitleCasePipe,
    PanelComponent,
    ContactFormComponent,
    NewCourseFormComponent,
    NewArticleFormComponent,
    TextComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    QuillModule
  ],
  providers: [
    ServerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
